CFLAGS  = `fltk-config --cxxflags`
LFLAGS   = `fltk-config --ldflags`

default: build

build: uzen.o
	g++ uzen.o -o uzen -std=c++0x ${LFLAGS}

uzen.o:
	g++ ${CFLAGS} -c uzen.cpp -std=c++0x

clean:
	rm -f uzen.o

install:
	chmod 775 uzen
	cp uzen /usr/local/bin

uninstall:
	rm -f /usr/local/bin/uzen
