// uzen 0.5
// g++ -std=c++0x `fltk-config --cxxflags` uzen.cpp `fltk-config --ldflags` -o uzen
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>

void mq(Fl_Widget *, void *) { exit(0); }

int main(int argc, char **argv) {
if (argc < 2) return 1;
const char *ti = "uzen";
unsigned short sw, sh, ww, wh, bh, bw, bs, bo = 0, ba = 40, fs = 25;
int e, lw = 0, lh = 0;
bool t = 0, f = 0, c = 0;
for (int n = 1; n < argc; ++n) {
if (!strcmp(argv[n], "-f")) {
if (n + 1 < argc && argv[n+1][0] > 47 && argv[n+1][0] < 58 && strlen(argv[n+1]) < 5) {
e = atoi(argv[++n]); if (e) { fs = e; } } }
else if (!strcmp(argv[n], "-t")) {
if (n + 1 < argc && argv[n+1][0] > 47 && argv[n+1][0] < 58 && strlen(argv[n+1]) < 5) {
e = atoi(argv[++n]); if (e) { ba = e; } } }
else if (!strcmp(argv[n], "-w")) f = 1;
else if (!strcmp(argv[n], "-c")) c = 1;
else if (!strcmp(argv[n], "-?")) { printf("\nuzen 0.5\n\nUSE: uzen text [win title] [options]\n\nOPTIONS:\n-f	font size\n-t	your title bar height\n-c	text center align\n-w	warning\n\n"); exit(0); }
else { if (!t) { t = 1; ti = argv[n]; } } }
sw = Fl::w();
sh = Fl::h();
bs = ( fs / (float)100 ) * 80;
bw = bs * 6;
bh = bs * 2.1;
if ( sw < 10 || sh < 10 ) sw = sh = 300;
fl_font(fl_font(), fs);
fl_measure(argv[1], lw, lh);
if (!lh) lh = fs;
ww = lw + 40;
if (ww+20 > sw) { ww = sw-40; lw = sw-80; }
if (lw < bw) { ww = bw+40; lw = bw; }
bo = ( sw - ww ) / 2;
if (lh + bh + 60 + ba > sh) lh = sh - 60 - bh - ba;
wh = lh + bh + 60;
Fl_Window *w = new Fl_Window(bo, 20+ba, ww, wh, ti);
Fl_Box *l = new Fl_Box(20, 20, lw, lh, argv[1]);
l->labelsize(fs);
if (f) l->labelcolor(fl_rgb_color(238, 84, 25));
if (!c) l->align( FL_ALIGN_INSIDE | FL_ALIGN_LEFT );
Fl_Button *b = new Fl_Button(ww-bw-20, lh+40, bw, bh, "Ok");
b->labelsize(bs);
b->callback(mq, 0);
w->end();
w->show(argc, argv);
return Fl::run(); }
